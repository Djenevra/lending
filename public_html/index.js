
  function contactForm(){
    let width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;

    let height = window.innerHeight
    || document.documentElement.clientHeight
    || document.body.clientHeight;

    if(height >= 600 && width >= 1024) {
      let form = document.getElementById('wrap-contact-form');
      form.style.display = form.style.display = 'block';
     
    }else{
      let form = document.getElementById('mobile-callback-wrapper');
      form.style.display = form.style.display = 'block';
    }
  }
 
  function closeWindow() {
    let body = document.getElementById('body')
    let form = document.getElementById('mobile-callback-wrapper');
    let nameField = document.getElementById("recall-name");
    let numberField = document.getElementById("recall-number");
    console.log(nameField, numberField);


    body.style.overflow = body.style.overflow = 'initial';
    form.style.display = form.style.display ='none';
    numberField.style.borderColor = "#D3D3D3";
    nameField.style.borderColor = "#D3D3D3";
    document.getElementById('mobile-callback-form').reset();
  }

  function replaceBlock() {
    let number = document.getElementById("phone-number--field").value
    let name = document.getElementById("name-field").value
    let phoneNumber = number.replace(/\s/g, '');
    let lengthNumber =  phoneNumber.length;
    let nameField = document.getElementById("name-field")
    let numberField = document.getElementById("phone-number--field")
    let newForm = document.getElementById('feedback');
    let oldForm = document.getElementById('business-form');


    if(number === '' && name === '')
    {
      nameField.style.borderColor="#FF0000";
      numberField.style.borderColor="#FF0000";
    }
    else if(name === '') {
      nameField.style.borderColor="#FF0000";
    }
    else if(number === ''
            || isNaN(phoneNumber)
            || lengthNumber < 10
            || phoneNumber[0] != 0)
    {
      numberField.style.borderColor="#FF0000";
    }
    else {
      oldForm.style.display = 'none';
      newForm.style.display = 'block';
    }
  };

  function validateData(event) {
    event.preventDefault();
    let number = document.getElementById("recall-number").value;
    let name = document.getElementById("recall-name").value;
    let phoneNumber = number.replace(/\s/g, '');
    let lengthNumber =  phoneNumber.length;
    let nameField = document.getElementById("recall-name");
    let numberField = document.getElementById("recall-number");
    let formwrapper = document.getElementById("mobile-callback-wrapper");
    let form = document.getElementById('mobile-callback-form')



    if(number === '' && name === '')
      {
        nameField.style.borderColor="#FF0000";
        numberField.style.borderColor="#FF0000";
      }
    else if(name === '') 
      {
      nameField.style.borderColor="#FF0000";
      }
    else if(number === ''
            || isNaN(phoneNumber)
            || lengthNumber < 10
            || phoneNumber[0] != 0)
      {
      numberField.style.borderColor="#FF0000";
      }
    else {
     formwrapper.style.display = "none";
     numberField.style.borderColor = "#D3D3D3";
     nameField.style.borderColor = "#D3D3D3";
     form.reset();
     enableScrollOnBody();
     
    }
  };

  function checkData(event) {
    event.preventDefault();
    let number = document.getElementById("phone-number").value;
    let phoneNumber = number.replace(/\s/g, '');
    let lengthNumber = phoneNumber.length;
    let name = document.getElementById("name").value;
    let nameField = document.getElementById("name");
    let numberField = document.getElementById("phone-number");
    let formwrapper = document.getElementById("wrap-contact-form")
    let form = document.getElementById('desktop-callback-form')


    if(number === '' && name === '')
      {
      nameField.style.borderColor="#FF0000";
      numberField.style.borderColor="#FF0000";
      }
    else if(name === '')
      {
     nameField.style.borderColor="#FF0000";
      }
    else if(number === ''
      || isNaN(phoneNumber)
      || lengthNumber < 10
      || phoneNumber[0] != 0)
    {
      numberField.style.borderColor="#FF0000";
    }
    else {
     formwrapper.style.display = "none";
     numberField.style.borderColor = "#D3D3D3";
     nameField.style.borderColor = "#D3D3D3";
     form.reset();
     enableScrollOnBody();
    }
  };

  function setAttributes(el, attrs) {
    for(var key in attrs) {
      el.setAttribute(key, attrs[key]);
    }
  }

  function addInput(event) {
    event.preventDefault();
    let newInputField = document.createElement('textarea')
    let uniqId = "id" + Math.random().toString(16).slice(2)
    setAttributes(newInputField, {'name': 'comment', 'cols': '1', 'rows': '1', 'class': 'subject-of--order--form', 'placeholder': 'Укажите что доставить'})
    let cross = document.createElement('div');
    setAttributes(cross, {'class' : 'input-cross', 'id': 'close-input-cross'})
    cross.setAttribute('onclick', 'closeInputField(this.parentNode.id)');
    let parentElement = document.getElementById("order-fields--container")
    let inputField = document.createElement('div');
    inputField.setAttribute('id', uniqId);
    inputField.setAttribute('class', "cross-container");
    newInputField.setAttribute("class", "add-item-container");
    inputField.innerHTML += cross.outerHTML + newInputField.outerHTML;
   // parentElement.insertBefore(inputField, parentElement.children[4]);
   parentElement.appendChild(inputField);
  }

  function closeInputField(id) {
    let inputField = document.getElementById(id);
    inputField.remove();
  }

  function addField(event) {
      event.preventDefault();
      let field = document.getElementById("additional-field");
      field.style.display ='block';
  }

  function displayCalendar(bool) {
    let calendar = document.getElementById("calendar");
  
    if (bool == true) {
      calendar.style.display = 'block'
    }else{
      calendar.style.display = 'none'
    };
  }

  function closeField() {
    let field = document.getElementById('additional-field');
    field.style.display ='none';
    document.getElementById('additional-info').reset();

  }


  function removeMobileForm() {
    var orientation = window.orientation;
    var deviceWidth = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;


    if (orientation == 90 && deviceWidth >= 768)
      {
        document.getElementById('mobile-callback-wrapper').style.display = 'none';
        enableScrollOnBody();
      } 
    else 
      { 
       document.getElementById('wrap-contact-form').style.display = 'none';
    
      }
  };
  

  window.addEventListener('orientationchange', removeMobileForm);

  

  //-------------- Disable scroll --------------//

  function enableScrollOnBody() {
    $('body').removeClass('scrollDisabled').css('margin-top', 0);
    $(window).scrollTop(y_offsetWhenScrollDisabled);
  }
  var y_offsetWhenScrollDisabled = 0;

  window.onload = function() {
  
    
   
  
    $('#contact-form-button').on('click', function() {
      disableScrollOnBody();
     contactForm();
    });

    $('#contact-form-close').on('click', function() {
      enableScrollOnBody();
      closeWindow();
    });
  
    function disableScrollOnBody() {
      y_offsetWhenScrollDisabled = $(window).scrollTop();
      $('body').addClass('scrollDisabled').css('margin-top', -y_offsetWhenScrollDisabled);
     let scrollBarWidth = window.innerWidth - window.innerWidth
      $('body').css({
      marginRight: scrollBarWidth,
      });
     
    }

  
    var DesktopBackCallButton = $('#desktop-btn');
   
    
    DesktopBackCallButton.on('click', function() {
  
    contactForm();
    });


  
    $(document).on('click', function(event) {
      let btnId = 'desktop-btn';
      let formId = 'contact-form';
      let nameField = 'name';
      let phoneField = 'phone-number';
      let desktopSubmitBtn = 'desktop-id-button';

      

         if (event.target.id != btnId
            && event.target.id != formId
            && event.target.id != nameField
            && event.target.id != phoneField
            && event.target.id != desktopSubmitBtn) {
             $("#wrap-contact-form").hide();
             $("#name").css("border", "#FF0000");
             $("#phone-number").css("border", "#FF0000");

          }
       })
   };

  

